#! / bin / bash
# -*- ENCODING: UTF-8 -*-

#author : Gian Franco Marcano ->gianf.marcano@gmail.com
#file : script.sh

#automate angularjs tasks

echo "running grunt --force"

grunt --force

echo "copying files from dist directory..."

cp -r dist/fonts/ ../public/
cp -r dist/images/ ../public/
cp -r dist/scripts/ ../public/
cp -r dist/styles/ ../public/
cp -r dist/404.html ../public/
cp -r dist/favicon.ico ../public/

cp -r dist/index.html ../resources/views/angularjs/

echo "files successfully copied to the public and resources directory"

#back
cd ..

echo "substituting '{{' for '@{{'..."

sed -r 's/\{\{/@{{/g' resources/views/angularjs/index.html > resources/views/angularjs/index.blade.php
 
#Delete index.html
rm -r resources/views/angularjs/"index.html" 

